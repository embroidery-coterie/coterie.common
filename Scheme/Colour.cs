﻿using System.Drawing;
using System.Globalization;

namespace Coterie.Common.Scheme
{
    public struct Colour
	{
		public int R { get; set; }
		public int G { get; set; }
		public int B { get; set; }

		public Colour(int R, int G, int B)
		{
			this.R = R;
			this.G = G;
			this.B = B;
		}

		public override string ToString()
		{
			var color = ToHtmlString();

            return color switch
            {
                "#ffffff" => "White",
				"#fffff9" => "Antique White",
				"#f1e3d1" => "Cream",
				"#e0d6cd" => "Platinum",
				"#e8d7c3" => "Vintage",
				"#ead5c6" => "Bone",
				"#d3c1b3" => "Mushroom",
				"#dac9b6" => "Light Mocha",
				"#fdcbae" => "Sandstone",
				"#e5b093" => "Amber",
				"#d7cac1" => "Taupe",
				"#d0bfb7" => "Natural",
				"#bfb29c" => "Dirty",
				"#ffa172" => "Tobasco",
				"#ff804d" => "Cognac",
				"#a86572" => "Raspberry",
				"#906586" => "Lavender Mist",
				"#dad8d3" => "Rue",
				"#9bc3ae" => "Antique Green",
				"#659d90" => "Teal",
				"#6f838e" => "French Blue",
				"#b4b4b9" => "New Blue",
				"#8397ae" => "Blue",
				"#000065" => "Navy Blue",
				"#008d40" => "Christmas Green",
				"#000000" => "Black",
				"#ff1e68" => "Christmas Red",
                _ => color,
            };
        }

		public string ToHtmlString()
        {
			var colorString = "#" + R.ToString("X2") + G.ToString("X2") + B.ToString("X2");
			return colorString.ToLower();
		}

		public static Colour FromHtmlString(string color)
        {
			var colorcode = color.TrimStart('#');

			if (colorcode.Length == 6)
				return new Colour(int.Parse(colorcode.Substring(0, 2), NumberStyles.HexNumber),
							      int.Parse(colorcode.Substring(2, 2), NumberStyles.HexNumber),
							      int.Parse(colorcode.Substring(4, 2), NumberStyles.HexNumber));
				return new Colour(
							int.Parse(colorcode.Substring(2, 2), NumberStyles.HexNumber),
							int.Parse(colorcode.Substring(4, 2), NumberStyles.HexNumber),
							int.Parse(colorcode.Substring(6, 2), NumberStyles.HexNumber));
		}

		public static bool operator ==(Colour a, Colour b)
        {
			if (a.R == b.R && a.G == b.G && a.B == b.B)
				return true;

			return false;
        }

		public static bool operator !=(Colour a, Colour b)
		{
			return !(a == b);
		}
	}
}