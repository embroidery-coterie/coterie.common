﻿namespace Coterie.Common.Scheme
{
    public class PatternMetrics
    {
        public int CanvasCount { get; private set; }
        public string CanvasType { get; private set; }
        public string CanvasColor { get; private set; }
        public int CanvasWidth { get; private set; }
        public int CanvasHeight { get; private set; }
        public string FlossVendor { get; private set; }
        public int FlossCount { get; private set; }
        public int StitchWidth { get; private set; }
        public int StitchHeight { get; private set; }
        public int TotalStitches { get; private set; }
        public bool HasBackStitches { get; private set; }
        public bool HasHalfStitches { get; private set; }
        public bool HasQuarterStitches { get; private set; }
        public bool HasPetitStitches { get; private set; }
        public bool HasThreeQuartersStitches { get; private set; }
        public bool HasBeads { get; private set; }
        public bool HasFrenchKnots { get; private set; }
        public bool HasBlends { get; private set; }

        public PatternMetrics(string flossVendor, int flossCount, int stitchWidth, int stitchHeight, int totalStitches, bool hasBackStitches, bool hasHalfStitches, bool hasQuarterStitches,
            bool hasPetitStitches, bool hasThreeQuartersStitches, bool hasBeads, bool hasFrenchKnots, bool hasBlends, int canvasCount, string canvasType, string canvasColor, int canvasWidth, int canvasHeight)
        {
            FlossVendor = flossVendor;
            FlossCount = flossCount;
            StitchWidth = stitchWidth;
            StitchHeight = stitchHeight;
            TotalStitches = totalStitches;
            HasBackStitches = hasBackStitches;
            HasHalfStitches = hasHalfStitches;
            HasQuarterStitches = hasQuarterStitches;
            HasPetitStitches = hasPetitStitches;
            HasThreeQuartersStitches = hasThreeQuartersStitches;
            HasBeads = hasBeads;
            HasFrenchKnots = hasFrenchKnots;
            HasBlends = hasBlends;
            CanvasCount = canvasCount;
            CanvasType = canvasType;
            CanvasColor = canvasColor;
            CanvasWidth = canvasWidth;
            CanvasHeight = canvasHeight;
        }
    }
}