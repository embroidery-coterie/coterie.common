﻿using System;

namespace Coterie.Common.Scheme
{
    public class PatternProgress
    {
        public Guid PatternId { get; private set; }

        public bool[] StitchMarks { get; set; }
        public bool[] BackStitchMarks { get; set; }
        public bool[] FrenchKnotMarks { get; set; }

        private Guid _version;
        public Guid Version => _version;

        private PatternProgress() { }

        public PatternProgress(Pattern pattern)
        {
            PatternId = pattern.Id;
            StitchMarks = new bool[pattern.Stitches.Count];
            BackStitchMarks = new bool[pattern.BackStitches.Count];
            FrenchKnotMarks = new bool[pattern.Nodes.Count];

            _version = Guid.NewGuid();
        }

        public void Clear()
        {
            StitchMarks = new bool[StitchMarks.Length];
            BackStitchMarks = new bool[BackStitchMarks.Length];
            FrenchKnotMarks = new bool[FrenchKnotMarks.Length];

            _version = Guid.NewGuid();
        }
    }
}