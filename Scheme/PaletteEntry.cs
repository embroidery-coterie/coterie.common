﻿using System.Text;

namespace Coterie.Common.Scheme
{
    public class PaletteEntry
	{
		public Colour Colour { get; set; }
		public Floss[] Flosses { get; set; }
		public string Symbol { get; set; }

        public override string ToString()
        {
            if (Flosses.Length == 1)
                return $"{Flosses[0].Vendor} {Flosses[0].Number}";

            var temp = new StringBuilder();

            for (int i = 0; i < Flosses.Length; i++)
                temp.AppendFormat("{0} {1} + ", Flosses[i].Vendor, Flosses[i].Number);

            var result = temp.ToString();

            return result.TrimEnd().TrimEnd('+').TrimEnd();
        }
    }
}