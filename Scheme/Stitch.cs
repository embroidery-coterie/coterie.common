﻿namespace Coterie.Common.Scheme
{
    public class Stitch
	{
		public int X { get; set; }
		public int Y { get; set; }
		public PaletteEntry PaletteEntry { get; set; }
		public StitchType Type { get; set; }

		public bool IsQuart
        {
			get => (int) Type >= 8 && (int)Type <= 11;
        }

		public bool IsThreeQuarter
		{
			get => (int)Type >= 4 && (int)Type <= 7;
		}

		public bool IsHalf
		{
			get => Type == StitchType.HalfTop || Type == StitchType.HalfBottom;
		}

		public bool IsPetit
		{
			get => (int)Type >= 12 && (int)Type <= 15;
		}
	}
}