﻿namespace Coterie.Common.Scheme
{
    public class Backstitch
	{
		public int X1 { get; set; }
		public int Y1 { get; set; }
		public int X2 { get; set; }
		public int Y2 { get; set; }
		public PaletteEntry PaletteEntry { get; set; }
		public int Strands { get; set; }
	}
}