﻿namespace Coterie.Common.Scheme
{
    public class Floss
	{
		public string Vendor { get; set; }
		public string Number { get; set; }
		public string Title { get; set; }
		public Colour Colour { get; set; }
		public int Strands { get; set; }
	}
}