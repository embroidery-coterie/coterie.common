﻿namespace Coterie.Common.Scheme
{
    public enum StitchType
	{
		undefined = 0,
		Full = 1,
		HalfTop = 2,
		HalfBottom = 3,
		ThreeQuarterTL = 4,
		ThreeQuarterTR = 5,
		ThreeQuarterBR = 6,
		ThreeQuarterBL = 7,
		QuarterTL = 8,
		QuarterTR = 9,
		QuarterBR = 10,
		QuarterBL = 11,
		PetitTL = 12,
		PetitTR = 13,
		PetitBR = 14,
		PetitBL = 15
	}
}