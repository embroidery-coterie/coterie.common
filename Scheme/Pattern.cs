﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Coterie.Common.Scheme
{
    public class Pattern
	{
		public Guid Id { get; set; }
		public Canvas Canvas { get; set; }
		public PatternInfo Info { get; set; }
		public List<PaletteEntry> Palette { get; set; }
		public List<Stitch> Stitches { get; set; }
		public List<Node> Nodes { get; set; }
		public List<Backstitch> BackStitches { get; set; }

		public Pattern(Guid id)
		{
			Id = id;
			Canvas = new Canvas();
			Palette = new List<PaletteEntry>();
			Stitches = new List<Stitch>();
			Nodes = new List<Node>();
			BackStitches = new List<Backstitch>();
		}

		public Dictionary<PaletteEntry, int> GetPaletteOrder()
		{
			var result = new Dictionary<PaletteEntry, int>();

			for (int i = 0; i < Palette.Count; i++)
				result.Add(Palette[i], i);

			return result;
		}

        public PatternMetrics GetMetrics()
        {
            var totalStitches = Stitches.Count + BackStitches.Count;

            var left = Stitches.Min(x => x.X);
            var right = Stitches.Max(x => x.X);
            var top = Stitches.Min(x => x.Y);
            var bottom = Stitches.Max(x => x.Y);

            var stitchWidth = right - left + 1;
            var stitchHeight = bottom - top + 1;

            var hasBackStitches = BackStitches.Count > 0;
            var hasHalfStitches = Stitches.Any(x => x.IsHalf);
            var hasQuarterStitches = Stitches.Any(x => x.IsQuart);
            var hasPetitStitches = Stitches.Any(x => x.IsPetit);
            var hasThreeQuartersStitches = Stitches.Any(x => x.IsThreeQuarter);
            var hasBeads = Nodes.Any(x => x.Type == NodeType.Bead);
            var hasFrenchKnots = Nodes.Any(x => x.Type == NodeType.FrenchKnot);
            var hasBlends = Palette.Any(x => x.Flosses.Length > 1);

            var flossCount = 0;
            var flossVendor = "None";

            if (Palette.Count > 0)
            {
                flossVendor = string.Empty;

                var vendors = new List<string>();
                var flossList = new List<string>();

                foreach (var paletteEntry in Palette)
                {
                    foreach (var floss in paletteEntry.Flosses)
                    {
                        flossList.Add($"{floss.Vendor}.{floss.Number}");
                        vendors.Add(floss.Vendor);
                        continue;
                    }
                }

                foreach (var uniqueVendor in vendors.Distinct())
                    flossVendor += $"{uniqueVendor}, ";

                flossVendor = flossVendor.TrimEnd().TrimEnd(',');
                flossCount = flossList.Distinct().Count();
            }

            var canvasMinWidth = stitchWidth + 4 * Canvas.Count;
            var canvasMinHeight = stitchHeight + 4 * Canvas.Count;

            return new PatternMetrics(flossVendor, flossCount, stitchWidth, stitchHeight, totalStitches, hasBackStitches, hasHalfStitches,
                hasQuarterStitches, hasPetitStitches, hasThreeQuartersStitches, hasBeads, hasFrenchKnots, hasBlends, Canvas.Count, Canvas.Type, 
                Canvas.Colour.ToString(), canvasMinWidth, canvasMinHeight);
        }
    }
}