﻿namespace Coterie.Common.Scheme.Format.V1
{
    public class PaletteEntry
	{
		public Colour Colour { get; set; }
		public Floss[] Flosses { get; set; }
		public string Symbol { get; set; }
	}
}