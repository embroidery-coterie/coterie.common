﻿namespace Coterie.Common.Scheme.Format.V1
{
    public class Node
	{
		public enum NodeType
		{
			FrenchKnot,
			Bead
		}

		public int X { get; set; }
		public int Y { get; set; }
		public int PaletteEntry { get; set; }
		public NodeType Type { get; set; }
	}
}