﻿namespace Coterie.Common.Scheme.Format.V1
{
    public struct PatternInfo
	{
		public string Title { get; set; }
		public string Author { get; set; }
		public string Company { get; set; }
		public string Copyright { get; set; }
	}
}