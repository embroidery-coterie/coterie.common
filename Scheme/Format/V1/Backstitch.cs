﻿namespace Coterie.Common.Scheme.Format.V1
{
    public class Backstitch
	{
		public int X1 { get; set; }
		public int Y1 { get; set; }
		public int X2 { get; set; }
		public int Y2 { get; set; }
		public int PaletteEntry { get; set; }
		public int Strands { get; set; }
	}
}