﻿namespace Coterie.Common.Scheme.Format.V1
{
    public class Stitch
	{
		public int X { get; set; }
		public int Y { get; set; }
		public int PaletteEntry { get; set; }
		public StitchType Type { get; set; }
	}
}