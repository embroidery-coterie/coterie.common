﻿namespace Coterie.Common.Scheme.Format.V1
{
    public class Canvas
	{
		public int Width { get; set; }
		public int Height { get; set; }
		public int Count { get; set; }
		public Colour Colour { get; set; }
		public string Type { get; set; }
	}
}

