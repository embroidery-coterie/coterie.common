﻿using System;
using System.Collections.Generic;

namespace Coterie.Common.Scheme.Format.V1
{
	public class Pattern
	{
		public Guid Id { get; set; }
		public Canvas Canvas { get; set; }
		public PatternInfo Info { get; set; }
		public List<PaletteEntry> Palette { get; set; }
		public List<Stitch> Stitches { get; set; }
		public List<Node> Nodes { get; set; }
		public List<Backstitch> BackStitches { get; set; }

		public Pattern(Guid id)
		{
			Id = id;
			Canvas = new Canvas();
			Palette = new List<PaletteEntry>();
			Stitches = new List<Stitch>();
			Nodes = new List<Node>();
			BackStitches = new List<Backstitch>();
		}
	}
}