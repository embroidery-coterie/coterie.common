﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Coterie.Common.Scheme.Format
{
    public class Adapter
    {
        private IJsonSerializer _jsonSerializer;

        public Adapter(IJsonSerializer jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }

        public Oxs.IOxsAdapter GetOxsAdapter(byte[] rawPatternData)
        {
            var stringPatternData = Encoding.UTF8.GetString(rawPatternData);
            stringPatternData = stringPatternData.Replace("< objecttype=", "<object objecttype=");

            var xdoc = XDocument.Parse(stringPatternData);

            var version = xdoc.Element("chart").Element("properties").Attribute("software_version").Value;
            var software = xdoc.Element("chart").Element("properties").Attribute("software").Value;

            switch (software)
            {
                case "Ursa Software":
                    return new Oxs.WinStitchAdapter();

                case "DP Software":
                    switch (version)
                    {
                        case "136": 
                            return new Oxs.CSPPAdapter136();
                        default:
                            return new Oxs.CSPPAdapter137();
                    }

                default:
                    throw new Exception($"Format is not supported: {software} / OXS{version}");
            }
        }

        public byte[] SerializeToV1(Pattern pattern)
        {
            var patternData = new V1.Pattern(pattern.Id)
            {
                Info = new V1.PatternInfo
                {
                    Author = pattern.Info.Author,
                    Company = pattern.Info.Company,
                    Copyright = pattern.Info.Copyright,
                    Title = pattern.Info.Title
                },

                Canvas = new V1.Canvas
                {
                    Colour = pattern.Canvas.Colour,
                    Width = pattern.Canvas.Width,
                    Height = pattern.Canvas.Height,
                    Count = pattern.Canvas.Count
                },

                Palette = pattern.Palette.Select(x => new V1.PaletteEntry
                {
                    Colour = x.Colour,
                    Symbol = x.Symbol,
                    Flosses = x.Flosses.Select(y => new V1.Floss
                    {
                        Colour = y.Colour,
                        Number = y.Number,
                        Strands = y.Strands,
                        Title = y.Title,
                         Vendor = y.Vendor
                    })
                    .ToArray()
                })
                .ToList()
            };

            patternData.Stitches = pattern.Stitches.Select(x => new V1.Stitch
            {
                X = x.X,
                Y = x.Y,
                PaletteEntry = pattern.Palette.IndexOf(x.PaletteEntry),
                Type = (V1.StitchType)Enum.Parse(typeof(V1.StitchType), x.Type.ToString())
            })
            .ToList();

            patternData.Nodes = pattern.Nodes.Select(x => new V1.Node
            {
                X = x.X,
                Y = x.Y,
                PaletteEntry = pattern.Palette.IndexOf(x.PaletteEntry),
                Type = (V1.Node.NodeType)Enum.Parse(typeof(V1.Node.NodeType), x.Type.ToString())
            })
            .ToList();

            patternData.BackStitches = pattern.BackStitches.Select(x => new V1.Backstitch
            {
                X1 = x.X1,
                Y1 = x.Y1,
                X2 = x.X2,
                Y2 = x.Y2,
                PaletteEntry = pattern.Palette.IndexOf(x.PaletteEntry),
                Strands = x.Strands
            })
            .ToList();

            var patternFile = new PatternFile
            {
                Version = 1,
                Data = Convert.ToBase64String(Encoding.UTF8.GetBytes(_jsonSerializer.Serialize(patternData)))
            };

            var stringData = _jsonSerializer.Serialize(patternFile);
            return CompressData(Encoding.UTF8.GetBytes(stringData));
        }

        public Pattern ConvertFrom(Xsd.Pattern pattern)
        {
            var result = new Pattern(pattern.Id)
            {
                Info = new PatternInfo
                {
                    Author = pattern.Info.Author,
                    Company = pattern.Info.Company,
                    Copyright = pattern.Info.Copyright,
                    Title = pattern.Info.Title
                },

                Canvas = new Canvas
                {
                    Colour = pattern.Canvas.Colour,
                    Width = pattern.Canvas.Width,
                    Height = pattern.Canvas.Height,
                    Count = pattern.Canvas.Count
                }
            };

            result.Palette = new List<PaletteEntry>();

            for (int i = 0; i < pattern.Palette.Count; i++)
            {
                var paletteEntry = new PaletteEntry
                {
                    Colour = pattern.Palette[i].Colour,
                    Symbol = pattern.Palette[i].Symbol
                };

                if (pattern.Palette[i].Blends == null || pattern.Palette[i].Blends.Count == 0)
                {
                    paletteEntry.Flosses = new Floss[]
                    {
                        new Floss
                        {
                            Colour = pattern.Palette[i].Colour,
                            Number = pattern.Palette[i].Number,
                            Strands = pattern.Palette[i].Strands.Full,
                            Title = pattern.Palette[i].Title,
                            Vendor = pattern.Palette[i].Vendor
                        }
                    };
                }
                else
                {
                    paletteEntry.Flosses = pattern.Palette[i].Blends.Select(x => new Floss
                    {
                        Colour = x.Colour,
                        Number = x.Number,
                        Strands = x.Strands.Full,
                        Vendor = x.Vendor
                    })
                    .ToArray();
                }

                result.Palette.Add(paletteEntry);
            }
            
            result.Stitches = pattern.Stitches.Select(x => new Stitch
            {
                X = (int)x.Offset / (int)pattern.Canvas.Height,
                Y = (int)x.Offset % (int)pattern.Canvas.Height,
                PaletteEntry = result.Palette[pattern.Palette.IndexOf(x.Floss)],
                Type = (StitchType)Enum.Parse(typeof(StitchType), x.Type.ToString())
            })
            .ToList();

            result.Nodes = pattern.Nodes.Select(x => new Node
            {
                X = x.X,
                Y = x.Y,
                PaletteEntry = result.Palette[pattern.Palette.IndexOf(x.Floss)],
                Type = (NodeType)Enum.Parse(typeof(NodeType), x.Type.ToString())
            })
            .ToList();

            result.BackStitches = pattern.BackStitches.Select(x => new Backstitch
            {
                X1 = x.X1,
                Y1 = x.Y1,
                X2 = x.X2,
                Y2 = x.Y2,
                PaletteEntry = result.Palette[pattern.Palette.IndexOf(x.Floss)],
                Strands = 1
            })
            .ToList();

            return result;
        }

        public Pattern ParsePattern(byte[] fileData)
        {
            var rawData = DecompressData(fileData);
            var patternFile = _jsonSerializer.Deserialize<PatternFile>(Encoding.UTF8.GetString(rawData));

            if (patternFile.Version != 1)
                throw new ArgumentException($"Pattern file v{patternFile.Version} is not supported");

            var data = Encoding.UTF8.GetString(Convert.FromBase64String(patternFile.Data));
            var patternV1 = _jsonSerializer.Deserialize<V1.Pattern>(data);

            if (patternV1 == null)
                return null;

            var result = new Pattern(patternV1.Id)
            {
                Info = new PatternInfo
                {
                    Author = patternV1.Info.Author,
                    Company = patternV1.Info.Company,
                    Copyright = patternV1.Info.Copyright,
                    Title = patternV1.Info.Title
                },

                Canvas = new Canvas
                {
                    Colour = patternV1.Canvas.Colour,
                    Width = patternV1.Canvas.Width,
                    Height = patternV1.Canvas.Height,
                    Count = patternV1.Canvas.Count
                },

                Palette = patternV1.Palette.Select(x => new PaletteEntry
                {
                    Colour = x.Colour,
                    Symbol = x.Symbol,
                    Flosses = x.Flosses.Select(y => new Floss
                    {
                        Colour = y.Colour,
                        Number = y.Number,
                        Strands = y.Strands,
                        Title = y.Title,
                        Vendor = y.Vendor
                    })
                    .ToArray()
                })
                .ToList()
            };

            result.Stitches = patternV1.Stitches.Select(x => new Stitch
            {
                X = x.X,
                Y = x.Y,
                PaletteEntry = result.Palette[x.PaletteEntry],
                Type = (StitchType)Enum.Parse(typeof(StitchType), x.Type.ToString())
            })
            .ToList();

            result.Nodes = patternV1.Nodes.Select(x => new Node
            {
                X = x.X,
                Y = x.Y,
                PaletteEntry = result.Palette[x.PaletteEntry],
                Type = (NodeType)Enum.Parse(typeof(NodeType), x.Type.ToString())
            })
            .ToList();

            result.BackStitches = patternV1.BackStitches.Select(x => new Backstitch
            {
                X1 = x.X1,
                Y1 = x.Y1,
                X2 = x.X2,
                Y2 = x.Y2,
                PaletteEntry = result.Palette[x.PaletteEntry],
                Strands = x.Strands
            })
            .ToList();

            return result;
        }

        public bool IsNativeFormat(byte[] rawData)
        {
            try
            {
                var data = DecompressData(rawData);
                var patternFile = _jsonSerializer.Deserialize<PatternFile>(Encoding.UTF8.GetString(data));

                if (patternFile.Data != null)
                    return true;

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private byte[] CompressData(byte[] data)
        {
            using var sourceStream = new MemoryStream(data);
            using var targetStream = new MemoryStream();
            using var gzip = new GZipStream(targetStream, System.IO.Compression.CompressionLevel.Optimal);
            sourceStream.CopyTo(gzip);
            gzip.Close();

            return targetStream.ToArray();
        }

        private byte[] DecompressData(byte[] data)
        {
            using var sourceStream = new MemoryStream(data);
            using var targetStream = new MemoryStream();
            using var gzip = new GZipStream(sourceStream, CompressionMode.Decompress);
            gzip.CopyTo(targetStream);
            gzip.Close();

            return targetStream.ToArray();
        }
    }
}