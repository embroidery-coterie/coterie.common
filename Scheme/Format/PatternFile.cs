﻿using System;
using System.Collections.Generic;

namespace Coterie.Common.Scheme.Format
{
	public class PatternFile
	{
		public int Version { get; set; }
		public string Data { get; set; }
	}
}