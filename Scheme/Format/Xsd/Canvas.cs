﻿namespace Coterie.Common.Scheme.Format.Xsd
{
    public class Canvas
	{
		public ushort Width { get; set; }
		public ushort Height { get; set; }
		public ushort Count { get; set; }
		public Colour Colour { get; set; }
		public string Type { get; set; }
	}
}

