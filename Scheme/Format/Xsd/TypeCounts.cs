﻿namespace Coterie.Common.Scheme.Format.Xsd
{
    public class TypeCounts
	{
		public int Full;

		public int Half;

		public int Quarter;

		public int ThreeQuarter;

		public int Petit;

		public int FrenchKnot;

		public int Bead;

		public int BackStitch;
	}
}