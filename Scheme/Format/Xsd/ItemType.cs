﻿namespace Coterie.Common.Scheme.Format.Xsd
{
    public enum ItemType
	{
		None = 0,
		Full = 1,
		Half = 2,
		Quarter = 4,
		ThreeQuarter = 8,
		Petit = 0x10,
		FrenchKnot = 0x20,
		BackStitch = 0x40,
		Bead = 0x80
	}
}