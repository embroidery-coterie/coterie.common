﻿namespace Coterie.Common.Scheme.Format.Xsd
{
    public class FlossBlend
	{
		public string Vendor { get; set; }
		public string Number { get; set; }
		public Colour Colour { get; set; }

		public FlossStrands Strands;
	}
}