﻿namespace Coterie.Common.Scheme.Format.Xsd
{
    public struct PatternInfo
	{
		public string Title;

		public string Author;

		public string Company;

		public string Copyright;
	}
}