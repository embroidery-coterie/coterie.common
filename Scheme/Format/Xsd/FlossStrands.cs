﻿namespace Coterie.Common.Scheme.Format.Xsd
{
    public struct FlossStrands
	{
		public int Full;

		public int Half;

		public int Quarter;

		public int ThreeQuarter;

		public int Petit;

		public int BackStitch;

		public int FrenchKnot;
	}
}