﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Coterie.Common.Scheme.Format.Xsd
{
	public class Pattern
	{
		public Guid Id { get; set; }
		public Canvas Canvas { get; set; }

		public FlossStrands Strands;
		public PatternInfo Info { get; set; }
		public List<Floss> Palette { get; set; }
		public List<Stitch> Stitches { get; set; }
		public List<Node> Nodes { get; set; }
		public List<Backstitch> BackStitches { get; set; }

		public int2 GetStitchPosition(Stitch stitch)
        {
			return new int2
			{ 
				x = (int)stitch.Offset / (int)Canvas.Height,
				y = (int)stitch.Offset % (int)Canvas.Height
			};
        }

		public Pattern(Guid id)
		{
			Id = id;
			Canvas = new Canvas();
			Palette = new List<Floss>();
			Stitches = new List<Stitch>();
			Nodes = new List<Node>();
			BackStitches = new List<Backstitch>();
		}

		public FlossStats[] GetFlossStats()
        {
			var stats = new List<FlossStats>();

            for (int i = 0; i < Stitches.Count; i++)
            {
				var stat = stats.FirstOrDefault(x => x.Floss == Stitches[i].Floss);

				if (stat == null)
                {
					stat = new FlossStats() { Floss = Stitches[i].Floss };
					stats.Add(stat);
                }

				switch(Stitches[i].ItemType)
                {
					case ItemType.Full:
						stat.FullStitches++;
						break;

					case ItemType.Half:
						stat.HalfStitches ++;
						break;

					case ItemType.Quarter:
						stat.QuarterStitches++;
						break;

					case ItemType.Petit:
						stat.PetitStitches++;
						break;
				}
            }

			for (int i = 0; i < BackStitches.Count; i++)
			{
				var stat = stats.FirstOrDefault(x => x.Floss == BackStitches[i].Floss);

				if (stat == null)
				{
					stat = new FlossStats() { Floss = BackStitches[i].Floss };
					stats.Add(stat);
				}

				stat.BackStitches++;
			}

			return stats.ToArray();
        }

		public Metrics GetMetrics()
        {
			var totalStitches = Stitches.Count + BackStitches.Count;

			var stitches = Stitches.Select(x => GetStitchPosition(x)).ToArray();

			var left = stitches.Min(x => x.x);
			var right = stitches.Max(x => x.x);
			var top = stitches.Min(x => x.y);
			var bottom = stitches.Max(x => x.y);

			var stitchWidth = right - left + 1;
			var stitchHeight = bottom - top + 1;

			var hasBackStitches = BackStitches.Count > 0;
			var hasHalfStitches = Stitches.Any(x => x.ItemType == ItemType.Half);
			var hasQuarterStitches = Stitches.Any(x => x.ItemType == ItemType.Quarter);
			var hasPetitStitches = Stitches.Any(x => x.ItemType == ItemType.Petit);
			var hasFrenchKnots = Stitches.Any(x => x.ItemType == ItemType.FrenchKnot);
			var hasBlends = Palette.Any(x => x.Blends.Count > 0);

			var flossCount = 0;
			var flossVendor = "None";

			if (Palette.Count > 0)
            {
				flossVendor = string.Empty;

				var vendors = new List<string>();
				var flossList = new List<string>();

				foreach (var floss in Palette)
                {
					if (floss.Blends.Count == 0)
					{
						flossList.Add($"{floss.Vendor}.{floss.Number}");
						vendors.Add(floss.Vendor);
						continue;
					}

					foreach (var blend in floss.Blends)
					{
						flossList.Add($"{blend.Vendor}.{blend.Number}");
						vendors.Add(blend.Vendor);
					}
				}

                foreach (var uniqueVendor in vendors.Distinct())
					flossVendor += $"{uniqueVendor}, ";

				flossVendor = flossVendor.TrimEnd().TrimEnd(',');
				flossCount = flossList.Distinct().Count();
			}

			var canvasMinWidth = stitchWidth + 4 * Canvas.Count;
			var canvasMinHeight = stitchHeight + 4 * Canvas.Count;

			return new Metrics(flossVendor, flossCount, stitchWidth, stitchHeight, totalStitches, hasBackStitches, hasHalfStitches, 
				hasQuarterStitches, hasPetitStitches, hasFrenchKnots, hasBlends, Canvas.Count, Canvas.Type, Canvas.Colour.ToString(), canvasMinWidth, canvasMinHeight);
		}

		public Dictionary<Floss, int> GetPaletteOrder()
        {
			var result = new Dictionary<Floss, int>();

			for (int i = 0; i < Palette.Count; i++)
				result.Add(Palette[i], i);

			return result;
		}
	}

	public class FlossStats
    {
		public Floss Floss { get; set; }

		public int FullStitches;
		public int HalfStitches;
		public int QuarterStitches;
		public int BackStitches;
		public int PetitStitches;
	}

	public class Metrics
    {
		public int CanvasCount { get; private set; }
		public string CanvasType { get; private set; }
		public string CanvasColor { get; private set; }
		public int CanvasWidth { get; private set; }
		public int CanvasHeight { get; private set; }
		public string FlossVendor { get; private set; }
        public int FlossCount { get; private set; }
		public int StitchWidth { get; private set; }
		public int StitchHeight { get; private set; }
		public int TotalStitches { get; private set; }
		public bool HasBackStitches { get; private set; }
		public bool HasHalfStitches { get; private set; }
		public bool HasQuarterStitches { get; private set; }
		public bool HasPetitStitches { get; private set; }
		public bool HasFrenchKnots { get; private set; }
		public bool HasBlends { get; private set; }

		public Metrics(string flossVendor, int flossCount, int stitchWidth, int stitchHeight, int totalStitches, bool hasBackStitches, bool hasHalfStitches, bool hasQuarterStitches,
            bool hasPetitStitches, bool hasFrenchKnots, bool hasBlends, int canvasCount, string canvasType, string canvasColor, int canvasWidth, int canvasHeight)
        {
            FlossVendor = flossVendor;
            FlossCount = flossCount;
            StitchWidth = stitchWidth;
            StitchHeight = stitchHeight;
            TotalStitches = totalStitches;
            HasBackStitches = hasBackStitches;
            HasHalfStitches = hasHalfStitches;
            HasQuarterStitches = hasQuarterStitches;
            HasPetitStitches = hasPetitStitches;
            HasFrenchKnots = hasFrenchKnots;
			HasBlends = hasBlends;
            CanvasCount = canvasCount;
			CanvasType = canvasType;
            CanvasColor = canvasColor;
            CanvasWidth = canvasWidth;
            CanvasHeight = canvasHeight;
        }
    }
}