﻿using System.Collections.Generic;

namespace Coterie.Common.Scheme.Format.Xsd
{
    public class Floss
	{
		public string Vendor { get; set; }
		public string Number { get; set; }
		public string Title { get; set; }
		public Colour Colour { get; set; }

		//public List<BlendColor> Blends = new List<BlendColor>();

		public FlossStrands Strands;

		public string Symbol { get; set; }
		public ItemType ContentTypeMap { get; set; }
		public string Description { get; set; }

		public bool HasStitches
		{
			get
			{
				if ((ContentTypeMap & ItemType.Full) != ItemType.Full && (ContentTypeMap & ItemType.Half) != ItemType.Half && (ContentTypeMap & ItemType.Quarter) != ItemType.Quarter && (ContentTypeMap & ItemType.ThreeQuarter) != ItemType.ThreeQuarter && (ContentTypeMap & ItemType.Petit) != ItemType.Petit)
				{
					return (ContentTypeMap & ItemType.FrenchKnot) == ItemType.FrenchKnot;
				}
				return true;
			}
		}

		public bool HasBackstitch => (ContentTypeMap & ItemType.BackStitch) == ItemType.BackStitch;

		public bool HasBeads => (ContentTypeMap & ItemType.Bead) == ItemType.Bead;

		public bool HasNodes
		{
			get
			{
				if ((ContentTypeMap & ItemType.Bead) != ItemType.Bead)
				{
					return (ContentTypeMap & ItemType.FrenchKnot) == ItemType.FrenchKnot;
				}
				return true;
			}
		}

		public List<FlossBlend> Blends;

		public Floss()
        {
			Blends = new List<FlossBlend>();
        }
	}
}