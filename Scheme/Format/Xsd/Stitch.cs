﻿namespace Coterie.Common.Scheme.Format.Xsd
{
    public class Stitch
	{
		public uint Offset;

		private Floss _floss;

		private StitchType _type;

		public Floss Floss
		{
			get
			{
				return _floss;
			}
			set
			{
				_floss = value;
				UpdateContentTypeMap();
			}
		}

		public StitchType Type
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
				UpdateContentTypeMap();
			}
		}

		public int Subdiv => SubdivByType(_type);

		public ItemType ItemType
		{
			get
			{
				switch (_type)
				{
					case StitchType.Full:
						return ItemType.Full;
					case StitchType.HalfTop:
					case StitchType.HalfBottom:
						return ItemType.Half;
					case StitchType.QuarterTL:
					case StitchType.QuarterTR:
					case StitchType.QuarterBR:
					case StitchType.QuarterBL:
						return ItemType.Quarter;
					case StitchType.ThreeQuarterTL:
					case StitchType.ThreeQuarterTR:
					case StitchType.ThreeQuarterBR:
					case StitchType.ThreeQuarterBL:
						return ItemType.ThreeQuarter;
					case StitchType.PetitTL:
					case StitchType.PetitTR:
					case StitchType.PetitBR:
					case StitchType.PetitBL:
						return ItemType.Petit;
					default:
						return ItemType.None;
				}
			}
		}

		public static int SubdivByType(StitchType type)
		{
			switch (type)
			{
				case StitchType.HalfBottom:
				case StitchType.ThreeQuarterTR:
				case StitchType.QuarterTR:
				case StitchType.PetitTR:
					return 1;
				case StitchType.ThreeQuarterBR:
				case StitchType.QuarterBR:
				case StitchType.PetitBR:
					return 2;
				case StitchType.ThreeQuarterBL:
				case StitchType.QuarterBL:
				case StitchType.PetitBL:
					return 3;
				default:
					return 0;
			}
		}

		private void UpdateContentTypeMap()
		{
			if (_floss != null && _type != 0)
			{
				switch (_type)
				{
					case StitchType.HalfTop:
					case StitchType.HalfBottom:
						_floss.ContentTypeMap |= ItemType.Half;
						break;
					case StitchType.PetitTL:
					case StitchType.PetitTR:
					case StitchType.PetitBR:
					case StitchType.PetitBL:
						_floss.ContentTypeMap |= ItemType.Petit;
						break;
					case StitchType.ThreeQuarterTL:
					case StitchType.ThreeQuarterTR:
					case StitchType.ThreeQuarterBR:
					case StitchType.ThreeQuarterBL:
						_floss.ContentTypeMap |= ItemType.ThreeQuarter;
						break;
					case StitchType.QuarterTL:
					case StitchType.QuarterTR:
					case StitchType.QuarterBR:
					case StitchType.QuarterBL:
						_floss.ContentTypeMap |= ItemType.Quarter;
						break;
					default:
						_floss.ContentTypeMap |= ItemType.Full;
						break;
				}
			}
		}
	}

	public enum StitchType
	{
		undefined = 0,
		Full = 1,
		HalfTop = 2,
		HalfBottom = 3,
		ThreeQuarterTL = 4,
		ThreeQuarterTR = 5,
		ThreeQuarterBR = 6,
		ThreeQuarterBL = 7,
		QuarterTL = 8,
		QuarterTR = 9,
		QuarterBR = 10,
		QuarterBL = 11,
		PetitTL = 12,
		PetitTR = 13,
		PetitBR = 14,
		PetitBL = 15
	}
}