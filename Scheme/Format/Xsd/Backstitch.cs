﻿using System;

namespace Coterie.Common.Scheme.Format.Xsd
{
    public class Backstitch
	{
		private Floss _floss;

		public ushort X1;

        public ushort Y1;

		public ushort X2;

		public ushort Y2;

		public Floss Floss
		{
			get
			{
				return _floss;
			}
			set
			{
				_floss = value;
				Floss.ContentTypeMap |= ItemType.BackStitch;
			}
		}

		public float CellCount => (float)Math.Sqrt(Math.Pow((double)(int)X2 - (double)(int)X1, 2.0) + Math.Pow((double)(int)Y2 - (double)(int)Y1, 2.0)) * 0.25f;
	}
}