﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Coterie.Common.Scheme.Format.Xsd
{
    public class XsdFormatAdapter
	{
		private uint ROL4(uint Val, uint Iterations)
		{
			uint num = 32u;
			uint num2 = Iterations % num;
			return (Val << (int)num2) | (Val >> (int)(num - num2));
		}

		private void ObfuscateRandomizer(ref uint[] RandVal1Ptr, ref uint RandVal2Ptr, ref uint[] Array)
		{
			uint num = 0u;
			RandVal2Ptr = ((RandVal1Ptr[3] & 0xFF) | ((BitConverter.GetBytes(RandVal1Ptr[2])[2] | (((RandVal1Ptr[0] << 8) | BitConverter.GetBytes(RandVal1Ptr[1])[1]) << 8)) << 8));
			byte[] array = new byte[16];
			for (int i = 0; i < 4; i++)
			{
				byte[] bytes = BitConverter.GetBytes(RandVal1Ptr[i]);
				for (int j = 0; j < 4; j++)
				{
					array[i * 4 + j] = bytes[j];
				}
			}
			do
			{
				Array[num] = (BitConverter.ToUInt32(array, (int)(4 * (num / 4u))) >> (int)num % 4) % 32u;
				num++;
			}
			while (num < 16);
		}

		private string ReadStringTrim(byte[] data, int offset, int length)
		{
			string @string = Encoding.Default.GetString(data, offset, length);
			return new Regex("\\0.*").Replace(@string, "");
		}

		public async Task<Pattern> ParsePattern(Guid id, byte[] data, bool notBlocking = true)
        {
			var pattern = new Pattern(id);
			var stream = new MemoryStream(data);

			return await ParsePattern(pattern, stream, notBlocking);
		}

		private async Task<Pattern> ParsePattern(string filePath)
		{
			var id = Guid.Parse(Path.GetFileName(filePath));
			var pattern = new Pattern(id);
			var stream = new MemoryStream();

			try
			{
				var filestream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
				filestream.CopyTo(stream);
			}
			catch
			{
				return null;
			}

			return await ParsePattern(pattern, stream);
		}

		private async Task<Pattern> ParsePattern(Pattern pattern, MemoryStream stream, bool notBlocking = true)
		{
			var rawdata = new byte[1024];

			stream.Read(rawdata, 0, 2);

			if (BitConverter.ToUInt16(rawdata, 0) != 1296)
			{
				return null;
			}

			stream.Seek(741L, SeekOrigin.Begin);
			stream.Read(rawdata, 0, 14);

			pattern.Canvas.Width = BitConverter.ToUInt16(rawdata, 0);
			pattern.Canvas.Height = BitConverter.ToUInt16(rawdata, 2);

			uint propCount = BitConverter.ToUInt32(rawdata, 4);
			ushort count = BitConverter.ToUInt16(rawdata, 8);

			pattern.Canvas.Count = BitConverter.ToUInt16(rawdata, 10);
			//pattern.ThreadCountY = BitConverter.ToUInt16(rawdata, 12);

			stream.Seek(761L, SeekOrigin.Begin);
			stream.Read(rawdata, 0, 1);

			int colorCount = rawdata[0];
			int count2 = 123;

			stream.Seek(763L, SeekOrigin.Begin);

			var stopwatch = Stopwatch.StartNew();

			for (int i = 0; i < colorCount; i++)
            {
                stream.Read(rawdata, 0, count2);

                var floss = new Floss();

                int key = rawdata[2];

                floss.Vendor = GetVendor(key);

                floss.Number = ReadStringTrim(rawdata, 3, 11);
                floss.Title = ReadStringTrim(rawdata, 14, 41);
                floss.Colour = new Colour(rawdata[55], rawdata[56], rawdata[57]);

                int num2 = BitConverter.ToUInt16(rawdata, 59);
                int num3 = 61;

                for (int j = 0; j < 4; j++)
                {
                    key = rawdata[num3];

					var vendor = GetVendor(key);

                    num3++;

                    string number = ReadStringTrim(rawdata, num3, 11);

                    num3 += 11;

                    if (j < num2)
                    {
						var blend = new FlossBlend
						{
							Vendor = vendor,
							Number = number,
                        };

						floss.Blends.Add(blend);
                    }
                }

                for (int k = 0; k < num2; k++)
                {
                    floss.Blends[k].Strands.Full = rawdata[num3];
                    floss.Blends[k].Strands.Half = rawdata[num3];
                    floss.Blends[k].Strands.Quarter = rawdata[num3];
                    floss.Blends[k].Strands.ThreeQuarter = rawdata[num3];
                    floss.Blends[k].Strands.Petit = rawdata[num3];
                    floss.Blends[k].Strands.FrenchKnot = rawdata[num3];
                    floss.Blends[k].Strands.BackStitch = rawdata[num3];
                    num3++;
                }

                pattern.Palette.Add(floss);
            }

            stream.Seek(colorCount * 2, SeekOrigin.Current);

			for (int l = 0; l < colorCount * 9; l++)
			{
				Math.Floor((float)l / 9f);
				_ = l % 9;

				stream.Read(rawdata, 0, 2);
				int num4 = BitConverter.ToUInt16(rawdata, 0);

				if (num4 > 0)
				{
					byte[] array2 = new byte[num4];
					stream.Read(array2, 0, num4);
					ReadStringTrim(array2, 0, num4);
				}
			}

			for (int m = 0; m < colorCount * 8; m++)
			{
				int index = (int)Math.Floor((float)m / 8f);
				int num5 = m % 8;

				stream.Read(rawdata, 0, 2);

				int num6 = BitConverter.ToUInt16(rawdata, 0);

				switch (num5)
				{
					case 0:
						pattern.Palette[index].Strands.Full = num6;
						break;
					case 1:
						pattern.Palette[index].Strands.Half = num6;
						break;
					case 2:
						pattern.Palette[index].Strands.Quarter = num6;
						break;
					case 3:
						pattern.Palette[index].Strands.BackStitch = num6;
						break;
					case 4:
						pattern.Palette[index].Strands.FrenchKnot = num6;
						break;
					case 5:
						pattern.Palette[index].Strands.Petit = num6;
						break;
				}
			}

			stream.Seek(2400L, SeekOrigin.Current);
			stream.Seek(2400L, SeekOrigin.Current);
			stream.Seek(960L, SeekOrigin.Current);
			stream.Seek(2400L, SeekOrigin.Current);
			stream.Seek(2400L, SeekOrigin.Current);
			stream.Seek(2400L, SeekOrigin.Current);
			stream.Seek(2400L, SeekOrigin.Current);

			ushort[] array3 = new ushort[colorCount];
			string[] array4 = new string[colorCount];

			for (int n = 0; n < 240; n++)
			{

				stream.Read(rawdata, 0, 53);

				if (n < colorCount)
				{
					array4[n] = ReadStringTrim(rawdata, 0, 33);
					array3[n] = 0;
				}
			}

			for (int num7 = 0; num7 < colorCount * 6; num7++)
			{
				stream.Read(rawdata, 0, 2);

				int num8 = (int)Math.Floor((float)num7 / 6f);
				int num9 = num7 % 6;

				if (num9 == 0 && rawdata[1] != byte.MaxValue && BitConverter.ToInt16(rawdata, 0) >= 0)
				{
					array3[num8] = BitConverter.ToUInt16(rawdata, 0);
				}

				if (num9 > 0 && array3[num8] == 0 && rawdata[1] != byte.MaxValue && BitConverter.ToInt16(rawdata, 0) >= 0)
				{
					array3[num8] = BitConverter.ToUInt16(rawdata, 0);
				}
			}

			stream.Read(rawdata, 0, 53);

			string text = ReadStringTrim(rawdata, 0, 33);

			stream.Seek(33L, SeekOrigin.Current);
			stream.Seek(2L, SeekOrigin.Current);
			stream.Seek(4L, SeekOrigin.Current);
			stream.Seek(28L, SeekOrigin.Current);
			stream.Seek(120L, SeekOrigin.Current);
			stream.Seek(120L, SeekOrigin.Current);
			stream.Seek(12L, SeekOrigin.Current);
			stream.Seek(2L, SeekOrigin.Current);
			stream.Seek(2L, SeekOrigin.Current);
			stream.Seek(101L, SeekOrigin.Current);
			stream.Read(rawdata, 0, 3);

			pattern.Canvas.Colour = new Colour(rawdata[0], rawdata[1], rawdata[2]);

			stream.Seek(65L, SeekOrigin.Current);
			stream.Read(rawdata, 0, 41);

			var info = new PatternInfo();

			info.Title = ReadStringTrim(rawdata, 0, 41);
			stream.Read(rawdata, 0, 41);

			info.Author = ReadStringTrim(rawdata, 0, 41);
			stream.Read(rawdata, 0, 41);

			info.Company = ReadStringTrim(rawdata, 0, 41);
			stream.Read(rawdata, 0, 201);

			info.Copyright = ReadStringTrim(rawdata, 0, 201);

			stream.Seek(2049L, SeekOrigin.Current);
			stream.Seek(6L, SeekOrigin.Current);
			stream.Read(rawdata, 0, 31);

			pattern.Canvas.Type = ReadStringTrim(rawdata, 0, 31);

			pattern.Info = info;

			stream.Seek(216L, SeekOrigin.Current);
			stream.Read(rawdata, 0, 14);

			pattern.Strands.Full = BitConverter.ToUInt16(rawdata, 0);
			pattern.Strands.Half = BitConverter.ToUInt16(rawdata, 2);
			pattern.Strands.Quarter = BitConverter.ToUInt16(rawdata, 4);
			pattern.Strands.BackStitch = BitConverter.ToUInt16(rawdata, 6);
			pattern.Strands.Petit = BitConverter.ToUInt16(rawdata, 8);
			pattern.Strands.FrenchKnot = 2;
			pattern.Strands.ThreeQuarter = 2;

			for (int num10 = 0; num10 < pattern.Palette.Count; num10++)
			{
				var cSColor2 = pattern.Palette[num10];

				//if (array3[num10] > 0)
				//{
				//	pattern.Palette[num10].Symbol = symbols[num10];
    //            }

				if (cSColor2.Strands.BackStitch == 0)
				{
					cSColor2.Strands.BackStitch = pattern.Strands.BackStitch;
				}

				if (cSColor2.Strands.FrenchKnot == 0)
				{
					cSColor2.Strands.FrenchKnot = pattern.Strands.FrenchKnot;
				}

				if (cSColor2.Strands.Full == 0)
				{
					cSColor2.Strands.Full = pattern.Strands.Full;
				}

				if (cSColor2.Strands.Half == 0)
				{
					cSColor2.Strands.Half = pattern.Strands.Half;
				}

				if (cSColor2.Strands.Petit == 0)
				{
					cSColor2.Strands.Petit = pattern.Strands.Petit;
				}

				if (cSColor2.Strands.Quarter == 0)
				{
					cSColor2.Strands.Quarter = pattern.Strands.Quarter;
				}

				if (cSColor2.Strands.ThreeQuarter == 0)
				{
					cSColor2.Strands.ThreeQuarter = pattern.Strands.ThreeQuarter;
				}
			}

			stream.Seek(16994L, SeekOrigin.Current);

			pattern = await LoadStitches(stream, propCount, pattern, notBlocking);

			stream.Seek(2L, SeekOrigin.Current);
			stream.Read(rawdata, 0, 2);

			ushort num12 = BitConverter.ToUInt16(rawdata, 0);

			for (int num13 = 0; num13 < num12; num13++)
			{
				stream.Read(rawdata, 0, 2);

				if (BitConverter.ToUInt16(rawdata, 0) != 4)
					continue;

				stream.Seek(2L, SeekOrigin.Current);
				stream.Read(rawdata, 0, 4);

				if (Encoding.ASCII.GetString(rawdata, 0, 4) != "sps1")
					continue;

				stream.Seek(256L, SeekOrigin.Current);
				stream.Seek(256L, SeekOrigin.Current);
				stream.Seek(2L, SeekOrigin.Current);

				for (int num14 = 0; num14 < 3; num14++)
				{
					stream.Read(rawdata, 0, 10);
					stream.Read(rawdata, 0, 2);

					if (BitConverter.ToUInt16(rawdata, 0) != 1296)
						break;

					stream.Read(rawdata, 0, 2);

					ushort num15 = BitConverter.ToUInt16(rawdata, 0);

					if (num15 > 0)
					{
						List<Node> nodes = new List<Node>();
						List<Backstitch> backstitches = new List<Backstitch>();
						pattern = await LoadJoints(stream, num15, pattern, notBlocking);
					}
				}
			}

			pattern = await LoadJoints(stream, count, pattern, notBlocking);
			stream.Close();

			//pattern = Apply34(pattern);

			return pattern;
		}

		//not working
		public Pattern Apply34(Pattern pattern)
        {
			var halfStitches = pattern.Stitches.Where(x => x.ItemType == ItemType.Half).ToArray();
			var quarterStitches = pattern.Stitches.Where(x => x.ItemType == ItemType.Quarter).ToArray();

			foreach (var halfStitch in halfStitches)
            {
				var quart = quarterStitches.FirstOrDefault(x => x.Floss.Number == halfStitch.Floss.Number && x.Offset == halfStitch.Offset);

				if (quart == null)
					continue;

				pattern.Stitches.RemoveAll(x => x.Type == halfStitch.Type && x.Offset == halfStitch.Offset && x.Subdiv == halfStitch.Subdiv);
				pattern.Stitches.RemoveAll(x => x.Type == quart.Type && x.Offset == quart.Offset && x.Subdiv == quart.Subdiv);

				var stitch34 = new Stitch
				{
					Type = StitchType.ThreeQuarterTR,
					Floss = halfStitch.Floss,
					Offset = halfStitch.Offset
				};

				pattern.Stitches.Add(stitch34);
			}


			return pattern;
		}

		private static string GetVendor(int key)
		{
			switch (key)
			{
				case 0: return "DMC";
				case 1: return "Anchor";
				case 2: return "Madeira";
				case 3: return "JPC";
				case 11: return "Riolis";
				case 12: return "DMC";
				case 18: return "Krein BF";
				case 19: return "Krein BVF";
				case 25: return "Caron WF";
				case 26: return "Caron WF";
				case 27: return "Caron Imp";
				case 28: return "Caron Imp";
				case 29: return "Caron WC";
				case 30: return "Caron WL";
				case 83: return "NeedleP";
				case 98: return "Gam";
				case 110: return "DMC";
				case 111: return "DMC";
				case 112: return "DMC";
				case 113: return "DMC";
				case 143: return "PNK";
				case 149: return "Dim";
				case 150: return "DMC";
				case 200: return "MH Glass";
				case 201: return "MH Antique ";
				case 202: return "MH Petite";
				case 203: return "MH Frosted";
				case 204: return "MH Crayon";
				case 205: return "MH Pebble";
				case 206: return "MH Magnifica";
				case 229: return "Bead";
				default: return "Unknown Vendor";
			}
		}

		private async Task<Pattern> LoadJoints(MemoryStream fs, ushort count, Pattern pattern, bool notBlocking)
		{
			var stopwatch = Stopwatch.StartNew();

			byte[] rawdata = new byte[512];

			for (int i = 0; i < count; i++)
			{
				fs.Read(rawdata, 0, 2);

				switch (BitConverter.ToUInt16(rawdata, 0))
				{
					case 2:
					case 5:
						{
							fs.Read(rawdata, 0, 12);
							Backstitch cSBackstitch = new Backstitch();
							cSBackstitch.X1 = BitConverter.ToUInt16(rawdata, 2);
							cSBackstitch.Y1 = BitConverter.ToUInt16(rawdata, 4);
							cSBackstitch.X2 = BitConverter.ToUInt16(rawdata, 6);
							cSBackstitch.Y2 = BitConverter.ToUInt16(rawdata, 8);
							if (rawdata[10] < pattern.Palette.Count)
							{
								cSBackstitch.Floss = pattern.Palette[rawdata[10]];
							}
							pattern.BackStitches.Add(cSBackstitch);
							break;
						}

					case 1:
						{
							Node cSNode2 = new Node();
							cSNode2.Type = Node.NodeType.FrenchKnot;
							fs.Read(rawdata, 0, 12);
							cSNode2.X = BitConverter.ToUInt16(rawdata, 2);
							cSNode2.Y = BitConverter.ToUInt16(rawdata, 4);
							if (rawdata[10] < pattern.Palette.Count)
							{
								cSNode2.Floss = pattern.Palette[rawdata[10]];
							}
							pattern.Nodes.Add(cSNode2);
							break;
						}

					case 6:
						{
							Node cSNode = new Node();
							cSNode.Type = Node.NodeType.Bead;
							fs.Read(rawdata, 0, 10);
							cSNode.X = BitConverter.ToUInt16(rawdata, 2);
							cSNode.Y = BitConverter.ToUInt16(rawdata, 4);
							if (rawdata[6] < pattern.Palette.Count)
							{
								cSNode.Floss = pattern.Palette[rawdata[6]];
							}
							pattern.Nodes.Add(cSNode);
							break;
						}

					case 4:
						fs.Seek(23L, SeekOrigin.Current);
						break;

					default:
						{
							fs.Seek(3L, SeekOrigin.Current);
							fs.Read(rawdata, 0, 2);
							ushort num = BitConverter.ToUInt16(rawdata, 0);
							fs.Seek(num * 4, SeekOrigin.Current);
							break;
						}
				}
			}

			return pattern;
		}

		private async Task<Pattern> LoadStitches(MemoryStream fs, uint propCount, Pattern pattern, bool notBlocking)
		{
			var stopwatch = Stopwatch.StartNew();

			byte[] array = new byte[32768];
			uint num = 1073741824u;
			uint num2 = 2147483648u;
			int num3 = pattern.Canvas.Width * pattern.Canvas.Height;

			List<uint> list = new List<uint>();
			uint RandVal2Ptr = 0u;
			uint[] Array = new uint[16];
			uint[] RandVal1Ptr = new uint[4];

			for (int i = 0; i < 4; i++)
			{
				fs.Read(array, 0, 4);
				RandVal1Ptr[i] = BitConverter.ToUInt32(array, 0);
			}

			ObfuscateRandomizer(ref RandVal1Ptr, ref RandVal2Ptr, ref Array);

			int num4 = 0;
			int num5 = 0;

			while (list.Count < num3)
			{
				fs.Read(array, 0, 4);
				uint num6 = BitConverter.ToUInt32(array, 0);
				if (num6 != 0)
				{
					fs.Read(array, 0, (int)(num6 * 4));
					uint[] array2 = new uint[num6];
					for (int j = 0; j < num6; j++)
					{
						array2[j] = BitConverter.ToUInt32(array, j * 4);
						array2[j] = (array2[j] ^ RandVal2Ptr ^ RandVal1Ptr[0]);
						RandVal2Ptr = ROL4(RandVal2Ptr, Array[num4]);
						RandVal1Ptr[0] += RandVal1Ptr[1];
						num4 = (num4 + 1) % 16;
					}
					int k = 0;
					int num7 = 1;
					for (; k < num6; k++)
					{
						num7 = 1;
						if ((array2[k] & num) != 0 && (array2[k] & num2) == 0)
						{
							num7 = (int)((array2[k] & 0x3FFFFFFF) >> 16);
							k++;
						}
						while (num7 > 0)
						{
							list.Add(array2[k]);
							num7--;
							num5++;
						}
					}
				}
				num5++;
			}
			List<byte[]> list2 = new List<byte[]>();

			for (int l = 0; l < propCount; l++)
			{
				byte[] array3 = new byte[10];
				fs.Read(array3, 0, 10);
				list2.Add(array3);
			}

			for (int m = 0; m < num3 && m < list.Count; m++)
			{
				byte[] bytes = BitConverter.GetBytes(list[m]);

				if (bytes[3] == 15)
					continue;

				int num8 = (int)Math.Floor((float)m / (float)(int)pattern.Canvas.Width);
				int offset = m % (int)pattern.Canvas.Width * pattern.Canvas.Height + num8;

				if ((bytes[3] & 0x80) != 0)
				{
					uint num9 = (list[m] & 0xFFFF) >> 11 << 15;
					uint num10 = ((list[m] >> 16) & 0x7FFF) + num9;

					if (num10 >= 0 && num10 < list2.Count)
					{
						byte[] array4 = list2[(int)num10];

						if ((array4[0] & 1) != 0)
						{
							Stitch cSStitch = new Stitch();
							cSStitch.Floss = pattern.Palette[array4[2]];
							cSStitch.Offset = (uint)offset;
							cSStitch.Type = StitchType.HalfTop;
							pattern.Stitches.Add(cSStitch);
						}

						if ((array4[0] & 2) != 0)
						{
							Stitch cSStitch2 = new Stitch();
							cSStitch2.Floss = pattern.Palette[array4[3]];
							cSStitch2.Offset = (uint)offset;
							cSStitch2.Type = StitchType.HalfBottom;
							pattern.Stitches.Add(cSStitch2);
						}

						if ((array4[0] & 4) != 0)
						{
							Stitch cSStitch3 = new Stitch();
							cSStitch3.Floss = pattern.Palette[array4[4]];
							cSStitch3.Offset = (uint)offset;
							cSStitch3.Type = StitchType.QuarterTL;
							pattern.Stitches.Add(cSStitch3);
						}

						if ((array4[0] & 0x10) != 0)
						{
							Stitch cSStitch4 = new Stitch();
							cSStitch4.Floss = pattern.Palette[array4[6]];
							cSStitch4.Offset = (uint)offset;
							cSStitch4.Type = StitchType.QuarterTR;
							pattern.Stitches.Add(cSStitch4);
						}

						if ((array4[0] & 0x20) != 0)
						{
							Stitch cSStitch5 = new Stitch();
							cSStitch5.Floss = pattern.Palette[array4[7]];
							cSStitch5.Offset = (uint)offset;
							cSStitch5.Type = StitchType.QuarterBR;
							pattern.Stitches.Add(cSStitch5);
						}

						if ((array4[0] & 8) != 0)
						{
							Stitch cSStitch6 = new Stitch();
							cSStitch6.Floss = pattern.Palette[array4[5]];
							cSStitch6.Offset = (uint)offset;
							cSStitch6.Type = StitchType.QuarterBL;
							pattern.Stitches.Add(cSStitch6);
						}

						if ((array4[1] & 1) != 0)
						{
							Stitch cSStitch7 = new Stitch();
							cSStitch7.Floss = pattern.Palette[array4[4]];
							cSStitch7.Offset = (uint)offset;
							cSStitch7.Type = StitchType.PetitTL;
							pattern.Stitches.Add(cSStitch7);
						}

						if ((array4[1] & 4) != 0)
						{
							Stitch cSStitch8 = new Stitch();
							cSStitch8.Floss = pattern.Palette[array4[6]];
							cSStitch8.Offset = (uint)offset;
							cSStitch8.Type = StitchType.PetitTR;
							pattern.Stitches.Add(cSStitch8);
						}

						if ((array4[1] & 8) != 0)
						{
							Stitch cSStitch9 = new Stitch();
							cSStitch9.Floss = pattern.Palette[array4[7]];
							cSStitch9.Offset = (uint)offset;
							cSStitch9.Type = StitchType.PetitBR;
							pattern.Stitches.Add(cSStitch9);
						}

						if ((array4[1] & 2) != 0)
						{
							Stitch cSStitch10 = new Stitch();
							cSStitch10.Floss = pattern.Palette[array4[5]];
							cSStitch10.Offset = (uint)offset;
							cSStitch10.Type = StitchType.PetitBL;
							pattern.Stitches.Add(cSStitch10);
						}
					}
				}
				else
				{
					Stitch cSStitch11 = new Stitch();
					cSStitch11.Floss = pattern.Palette[bytes[2]];
					cSStitch11.Offset = (uint)offset;
					cSStitch11.Type = StitchType.Full;
					pattern.Stitches.Add(cSStitch11);
				}
			}

			return pattern;
		}
	}
}