﻿namespace Coterie.Common.Scheme.Format.Xsd
{
    public class Node
	{
		public enum NodeType
		{
			Undefined,
			FrenchKnot,
			Bead
		}

		public ushort X { get; set; }

		public ushort Y { get; set; }

		private Floss _floss;

		private NodeType _nodeType;

		public Floss Floss
		{
			get
			{
				return _floss;
			}
			set
			{
				_floss = value;
				SetColorItems();
			}
		}

		public ItemType ItemType
		{
			get
			{
				if (Type == NodeType.Bead)
				{
					return ItemType.Bead;
				}
				return ItemType.FrenchKnot;
			}
		}

		public NodeType Type
		{
			get
			{
				return _nodeType;
			}
			set
			{
				_nodeType = value;
				SetColorItems();
			}
		}

		private void SetColorItems()
		{
			if (Floss != null && Type != 0)
			{
				if (Type == NodeType.Bead)
					Floss.ContentTypeMap |= ItemType.Bead;
				else
					Floss.ContentTypeMap |= ItemType.FrenchKnot;
			}
		}
	}
}