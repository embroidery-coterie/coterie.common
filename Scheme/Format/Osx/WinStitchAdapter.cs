﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Xml.Linq;

namespace Coterie.Common.Scheme.Format.Oxs
{
    public class WinStitchAdapter : IOxsAdapter
    {
        public Pattern ParsePattern(Guid id, byte[] rawdata)
        {
            var stringData = Encoding.UTF8.GetString(rawdata);

            var pattern = new Pattern(id);

            var doc = XDocument.Parse(stringData);
            var root = doc.Element("chart");
            var formatElement = root.Element("format");
            var propertiesElement = root.Element("properties");


            pattern.Info = new PatternInfo
            {
                Copyright = propertiesElement.Attribute("copyright").Value,
                Author = propertiesElement.Attribute("author").Value,
                Title = propertiesElement.Attribute("charttitle").Value,
            };

            var paletteNodeRoot = root.Element("palette");
            var canvasNode = paletteNodeRoot.Elements("palette_item").FirstOrDefault(x => x.Attribute("number").Value == "cloth");

            pattern.Canvas = new Canvas
            {
                Count = Convert.ToInt32(propertiesElement.Attribute("stitchesperinch").Value),
                Height = Convert.ToInt32(propertiesElement.Attribute("chartheight").Value),
                Width = Convert.ToInt32(propertiesElement.Attribute("chartwidth").Value),
                Type = "Aida",
                Colour = Colour.FromHtmlString(canvasNode.Attribute("color").Value)
            };

            pattern.Palette = new List<PaletteEntry>();

            var paletteDictionary = new Dictionary<int, int>();

            foreach (var paletteNode in paletteNodeRoot.Elements("palette_item"))
            {
                if (paletteNode.Attribute("name").Value == "cloth")
                    continue;

                var hasBlend = paletteNode.Attribute("blendcolor").Value != "nil";

                var paletteEntry = new PaletteEntry
                {
                    Colour = Colour.FromHtmlString(paletteNode.Attribute("color").Value),
                    Flosses = new Floss[hasBlend ? 2 : 1]
                };

                var colorData = paletteNode.Attribute("number").Value
                    .Split(" ")
                    .Where(x => x.Length != 0)
                    .ToArray();

                paletteEntry.Flosses[0] = new Floss
                {
                    Vendor = colorData[0].Trim(),
                    Number = colorData[1].Trim(),
                    Colour = paletteEntry.Colour,
                    Strands = Convert.ToInt32(paletteNode.Attribute("strands").Value)
                };

                if (hasBlend)
                {
                    var blendData = paletteNode.Attribute("name").Value
                                            .Split(" ")
                                            .Where(x => x.Length != 0)
                                            .ToArray();

                    paletteEntry.Flosses[1] = new Floss
                    {
                        Vendor = blendData[^2].Trim(),
                        Number = blendData[^1].Trim(),
                        Colour = Colour.FromHtmlString(paletteNode.Attribute("blendcolor").Value),
                        Strands = Convert.ToInt32(paletteNode.Attribute("bsstrands").Value)
                    };
                }

                paletteDictionary.Add(Convert.ToInt32(paletteNode.Attribute("index").Value), pattern.Palette.Count);
                pattern.Palette.Add(paletteEntry);
            }

            pattern.Stitches = new List<Stitch>();

            foreach (var stitchNode in root.Element("fullstitches").Elements("stitch"))
            {
                var paliindex = stitchNode.Attribute("palindex").Value;

                pattern.Stitches.Add(new Stitch
                {
                    PaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(paliindex)]],
                    Type = StitchType.Full,
                    X = Convert.ToInt32(stitchNode.Attribute("x").Value),
                    Y = Convert.ToInt32(stitchNode.Attribute("y").Value)
                });
            }

            foreach (var stitchNode in root.Element("partstitches").Elements("partstitch"))
            {
                var partType1 = StitchType.ThreeQuarterBL;
                var partType2 = StitchType.ThreeQuarterBL;

                switch (stitchNode.Attribute("direction").Value)
                {
                    case "1":
                        partType1 = StitchType.ThreeQuarterBL;
                        partType2 = StitchType.ThreeQuarterTR;
                        break;

                    case "2":
                        partType1 = StitchType.ThreeQuarterTL;
                        partType2 = StitchType.ThreeQuarterBR;
                        break;

                    case "3":
                        partType1 = StitchType.ThreeQuarterBR;
                        partType2 = StitchType.ThreeQuarterTL;
                        break;

                    case "4":
                        partType1 = StitchType.ThreeQuarterTR;
                        partType2 = StitchType.ThreeQuarterBL;
                        break;
                }

                if (stitchNode.Attribute("palindex1").Value != "0")
                {
                    pattern.Stitches.Add(new Stitch
                    {
                        PaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(stitchNode.Attribute("palindex1").Value)]],
                        Type = partType1,
                        X = Convert.ToInt32(stitchNode.Attribute("x").Value),
                        Y = Convert.ToInt32(stitchNode.Attribute("y").Value)
                    });
                }

                if (stitchNode.Attribute("palindex2").Value != "0")
                {
                    pattern.Stitches.Add(new Stitch
                    {
                        PaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(stitchNode.Attribute("palindex2").Value)]],
                        Type = partType2,
                        X = Convert.ToInt32(stitchNode.Attribute("x").Value),
                        Y = Convert.ToInt32(stitchNode.Attribute("y").Value)
                    });
                }
            }

            pattern.BackStitches = new List<Backstitch>();



            Line line = null;
            PaletteEntry bsPaletteEntry = null;

            foreach (var stitchNode in root.Element("backstitches").Elements("backstitch"))
            {
                float x1 = Convert.ToSingle(stitchNode.Attribute("x1").Value, CultureInfo.InvariantCulture);
                float x2 = Convert.ToSingle(stitchNode.Attribute("x2").Value, CultureInfo.InvariantCulture);
                float y1 = Convert.ToSingle(stitchNode.Attribute("y1").Value, CultureInfo.InvariantCulture);
                float y2 = Convert.ToSingle(stitchNode.Attribute("y2").Value, CultureInfo.InvariantCulture);

                var paliindex = stitchNode.Attribute("palindex").Value;

                if (line == null)
                {
                    line = new Line
                    {
                        a = new Vector2(x1, y1),
                        b = new Vector2(x2, y2)
                    };

                    bsPaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(paliindex)]];

                    continue;
                }
                else
                {
                    var nextLine = new Line
                    {
                        a = new Vector2(x1, y1),
                        b = new Vector2(x2, y2)
                    };

                    var nextPaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(paliindex)]];

                    if (bsPaletteEntry.Colour != nextPaletteEntry.Colour ||
                        line.b != nextLine.a ||
                        !line.AreCodirected(nextLine))
                    {
                        pattern.BackStitches.Add(new Backstitch
                        {
                            PaletteEntry = bsPaletteEntry,
                            Strands = 1,
                            X1 = Convert.ToInt32(line.a.X * 2),
                            Y1 = Convert.ToInt32(line.a.Y * 2),
                            X2 = Convert.ToInt32(line.b.X * 2),
                            Y2 = Convert.ToInt32(line.b.Y * 2)
                        });

                        line = nextLine;
                        bsPaletteEntry = nextPaletteEntry;

                        continue;
                    }
                    else
                    {
                        line.b = nextLine.b;
                    }
                }

            }

            if (line != null)
            {
                pattern.BackStitches.Add(new Backstitch
                {
                    PaletteEntry = bsPaletteEntry,
                    Strands = 1,
                    X1 = Convert.ToInt32(line.a.X * 2),
                    Y1 = Convert.ToInt32(line.a.Y * 2),
                    X2 = Convert.ToInt32(line.b.X * 2),
                    Y2 = Convert.ToInt32(line.b.Y * 2)
                });
            }

            return pattern;
        }

        public class Line
        {
            public Vector2 a;
            public Vector2 b;

            public bool AreCodirected(Line line)
            {
                var v1 = b - a;
                var v2 = line.b - line.a;

                v1 = Vector2.Normalize(v1);
                v2 = Vector2.Normalize(v2);

                return Vector2.Dot(v1, v2) > 1 - 0.00001f;
            }
        }

    }
}