﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Xml.Linq;

namespace Coterie.Common.Scheme.Format.Oxs
{
    public class CSPPAdapter136 : IOxsAdapter
    {
        public Pattern ParsePattern(Guid id, byte[] rawdata)
        {
            var stringData = Encoding.UTF8.GetString(rawdata);
            stringData = stringData.Replace("< objecttype=", "<object objecttype=");

            var pattern = new Pattern(id);

            var doc = XDocument.Parse(stringData);
            var root = doc.Element("chart");
            var formatElement = root.Element("format");
            var propertiesElement = root.Element("properties");

            var copyrightAttribute = propertiesElement.Attributes().FirstOrDefault(x => x.Name == "copyright");
            var authorAttribute = propertiesElement.Attributes().FirstOrDefault(x => x.Name == "author");
            var titleAttribute = propertiesElement.Attributes().FirstOrDefault(x => x.Name == "charttitle");

            pattern.Info = new PatternInfo
            {
                Copyright = copyrightAttribute != null ? copyrightAttribute.Value : "Unknown",
                Author = authorAttribute != null ? authorAttribute.Value : "Unknown",
                Title = titleAttribute != null ? titleAttribute.Value : "Unknown",
            };

            var paletteNodeRoot = root.Element("palette");
            var canvasNode = paletteNodeRoot.Elements("palette_item").FirstOrDefault(x => x.Attribute("number").Value == "cloth");

            pattern.Canvas = new Canvas
            {
                Count = Convert.ToInt32(propertiesElement.Attribute("stitchesperinch").Value),
                Height = Convert.ToInt32(propertiesElement.Attribute("chartheight").Value),
                Width = Convert.ToInt32(propertiesElement.Attribute("chartwidth").Value),
                Type = "Aida",
                Colour = Colour.FromHtmlString(canvasNode.Attribute("color").Value)
            };

            pattern.Palette = new List<PaletteEntry>();

            var paletteDictionary = new Dictionary<int, int>();

            /*
             chart><properties oxsversion="1.0" software="DP Software" software_version="137" chartheight="200" chartwidth="300"
            stitchesperinch="14" stitchesperinch_y="14" palettecount="3"/><palette filename="Anchor Tapestry.rng" shortname="ANCT">
            <palette_item index="0" strands="2" bsstrands="1" number="cloth" name="cloth" color="E5D9C2" bscolor="000000" printcolor="000000" 
            symbolcolor="000000" colorcmyk="00050F0A" bscolorcmyk="00000064" printcolorcmyk="00000064" symbol="0" fontname="Cross Stitch Pro Platinum"/>
            <palette_item index="1" strands="2" bsstrands="1" number="ANCT 8394" name="Rose Pink" color="EB9999" bscolor="000000" printcolor="000000" symbolcolor="000000" colorcmyk="00222208" bscolorcmyk="00000064" printcolorcmyk="00000064" symbol="1" fontname="Cross Stitch Pro Platinum"/><palette_item index="2" strands="2" bsstrands="1" number="ANCT 8586" name="Lilac" color="A18CB3" bscolor="000000" printcolor="000000" symbolcolor="000000" colorcmyk="0A15001E" bscolorcmyk="00000064" printcolorcmyk="00000064" symbol="2" fontname="Cross Stitch Pro Platinum"/><palette_item index="3" strands="2" bsstrands="1" number="ANCT 8644" name="China Blue" color="5970AD" bscolor="000000" printcolor="000000" symbolcolor="000000" colorcmyk="30230020" bscolorcmyk="00000064" printcolorcmyk="00000064" symbol="3" fontname="Cross Stitch Pro Platinum"/></palette>
             */

            foreach (var paletteNode in paletteNodeRoot.Elements("palette_item"))
            {
                var colorDataString = paletteNode.Attribute("number").Value;

                if (colorDataString == "cloth")
                    continue;

                if (colorDataString.Contains("Unused"))
                    continue;

                var colorData = colorDataString
                    .Trim()
                    .Split(" ")
                    .ToArray();

                var vendor = "DMC";
                var numbers = colorData[0].Split("+");

                var paletteEntry = new PaletteEntry
                {
                    Colour = Colour.FromHtmlString(paletteNode.Attribute("color").Value),
                    Flosses = new Floss[numbers.Length]
                };

                for (int i = 0; i < numbers.Length; i++)
                {
                    paletteEntry.Flosses[i] = new Floss
                    {
                        Vendor = vendor,
                        Number = numbers[i],
                        Colour = paletteEntry.Colour,
                        Strands = numbers.Length > 1 ? 1 : Convert.ToInt32(paletteNode.Attribute("strands").Value)
                    };
                }

                paletteDictionary.Add(Convert.ToInt32(paletteNode.Attribute("index").Value), pattern.Palette.Count);
                pattern.Palette.Add(paletteEntry);
            }

            pattern.Stitches = new List<Stitch>();

            foreach (var stitchNode in root.Element("fullstitches").Elements("stitch"))
            {
                var paliindex = stitchNode.Attribute("palindex").Value;

                pattern.Stitches.Add(new Stitch
                {
                    PaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(paliindex)]],
                    Type = StitchType.Full,
                    X = Convert.ToInt32(stitchNode.Attribute("x").Value),
                    Y = Convert.ToInt32(stitchNode.Attribute("y").Value)
                });
            }

            foreach (var stitchNode in root.Element("partstitches").Elements("partstitch"))
            {
                var partType1 = StitchType.ThreeQuarterBL;
                var partType2 = StitchType.ThreeQuarterBL;

                switch (stitchNode.Attribute("direction").Value)
                {
                    case "1":
                        partType1 = StitchType.ThreeQuarterBL;
                        partType2 = StitchType.ThreeQuarterTR;
                        break;

                    case "2":
                        partType1 = StitchType.ThreeQuarterTL;
                        partType2 = StitchType.ThreeQuarterBR;
                        break;

                    case "3":
                        partType1 = StitchType.ThreeQuarterBR;
                        partType2 = StitchType.ThreeQuarterTL;
                        break;

                    case "4":
                        partType1 = StitchType.ThreeQuarterTR;
                        partType2 = StitchType.ThreeQuarterBL;
                        break;
                }

                var colorIndex1 = stitchNode.Attribute("palindex1").Value;
                var colorIndex2 = stitchNode.Attribute("palindex2").Value;

                if (colorIndex1 != "0")
                {
                    pattern.Stitches.Add(new Stitch
                    {
                        PaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(colorIndex1)]],
                        Type = partType1,
                        X = Convert.ToInt32(stitchNode.Attribute("x").Value),
                        Y = Convert.ToInt32(stitchNode.Attribute("y").Value)
                    });
                }

                if (colorIndex2 != "0")
                {
                    pattern.Stitches.Add(new Stitch
                    {
                        PaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(colorIndex2)]],
                        Type = partType2,
                        X = Convert.ToInt32(stitchNode.Attribute("x").Value),
                        Y = Convert.ToInt32(stitchNode.Attribute("y").Value)
                    });
                }
            }

            pattern.BackStitches = new List<Backstitch>();

            Line line = null;
            PaletteEntry bsPaletteEntry = null;

            foreach (var stitchNode in root.Element("backstitches").Elements("backstitch"))
            {
                var paliindex = stitchNode.Attribute("palindex").Value;

                float x1 = Convert.ToSingle(stitchNode.Attribute("x1").Value.Replace(",", "."), CultureInfo.InvariantCulture);
                float x2 = Convert.ToSingle(stitchNode.Attribute("x2").Value.Replace(",", "."), CultureInfo.InvariantCulture);
                float y1 = Convert.ToSingle(stitchNode.Attribute("y1").Value.Replace(",", "."), CultureInfo.InvariantCulture);
                float y2 = Convert.ToSingle(stitchNode.Attribute("y2").Value.Replace(",", "."), CultureInfo.InvariantCulture);

                if (line == null)
                {
                    line = new Line
                    {
                        a = new Vector2(x1, y1),
                        b = new Vector2(x2, y2)
                    };

                    bsPaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(paliindex)]];

                    continue;
                }
                else
                {
                    var nextLine = new Line
                    {
                        a = new Vector2(x1, y1),
                        b = new Vector2(x2, y2)
                    };

                    var nextPaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(paliindex)]];

                    if (bsPaletteEntry.Colour != nextPaletteEntry.Colour ||
                        line.b != nextLine.a ||
                        !line.AreCodirected(nextLine))
                    {
                        pattern.BackStitches.Add(new Backstitch
                        {
                            PaletteEntry = bsPaletteEntry,
                            Strands = 1,
                            X1 = Convert.ToInt32(line.a.X * 2),
                            Y1 = Convert.ToInt32(line.a.Y * 2),
                            X2 = Convert.ToInt32(line.b.X * 2),
                            Y2 = Convert.ToInt32(line.b.Y * 2)
                        });

                        line = nextLine;
                        bsPaletteEntry = nextPaletteEntry;

                        continue;
                    }
                    else
                    {
                        line.b = nextLine.b;
                    }
                }

            }

            if (line != null)
            {
                pattern.BackStitches.Add(new Backstitch
                {
                    PaletteEntry = bsPaletteEntry,
                    Strands = 1,
                    X1 = Convert.ToInt32(line.a.X * 2),
                    Y1 = Convert.ToInt32(line.a.Y * 2),
                    X2 = Convert.ToInt32(line.b.X * 2),
                    Y2 = Convert.ToInt32(line.b.Y * 2)
                });
            }

            pattern.Nodes = new List<Node>();

            foreach (var specialNode in root.Element("ornaments_inc_knots_and_beads").Elements("object"))
            {
                var paliindexString = specialNode.Attribute("palindex").Value;
                var typeString = specialNode.Attribute("objecttype").Value;
                var x1String = specialNode.Attribute("x1").Value;
                var y1String = specialNode.Attribute("y1").Value;
                StitchType type = default;

                if (typeString == "quarter" && specialNode.Attributes().Any(x => x.Name == "petit"))
                {
                    var petit = Convert.ToBoolean(specialNode.Attribute("petit").Value);

                    if (petit)
                        typeString = "petit";
                }

                switch (typeString)
                {
                    case "quarter":
                        type = StitchType.QuarterTL;

                        if (x1String.Contains(".5") && y1String.Contains(".5"))
                            type = StitchType.QuarterBR;
                        else if (x1String.Contains(".5"))
                            type = StitchType.QuarterTR;
                        else if (y1String.Contains(".5"))
                            type = StitchType.QuarterBL;

                        x1String = x1String.Replace(".5", "");
                        y1String = y1String.Replace(".5", "");

                        pattern.Stitches.Add(new Stitch
                        {
                            PaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(paliindexString)]],
                            Type = type,
                            X = Convert.ToInt32(x1String),
                            Y = Convert.ToInt32(y1String)
                        });

                        break;

                    case "petit":
                        type = StitchType.PetitTL;

                        if (x1String.Contains(".5") && y1String.Contains(".5"))
                            type = StitchType.PetitBR;
                        else if (x1String.Contains(".5"))
                            type = StitchType.PetitTR;
                        else if (y1String.Contains(".5"))
                            type = StitchType.PetitBL;

                        x1String = x1String.Replace(".5", "");
                        y1String = y1String.Replace(".5", "");

                        pattern.Stitches.Add(new Stitch
                        {
                            PaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(paliindexString)]],
                            Type = type,
                            X = Convert.ToInt32(x1String),
                            Y = Convert.ToInt32(y1String)
                        });

                        break;

                    case "tent":
                        var directionString = specialNode.Attribute("direction").Value;
                        type = directionString == "1" ? StitchType.HalfTop : StitchType.HalfBottom;

                        pattern.Stitches.Add(new Stitch
                        {
                            PaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(paliindexString)]],
                            Type = type,
                            X = Convert.ToInt32(x1String),
                            Y = Convert.ToInt32(y1String)
                        });

                        break;

                    case "knot":
                        pattern.Nodes.Add(new Node
                        {
                            PaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(paliindexString)]],
                            Type = NodeType.FrenchKnot,
                            X = Convert.ToInt32(Convert.ToSingle(x1String, CultureInfo.InvariantCulture) * 2f),
                            Y = Convert.ToInt32(Convert.ToSingle(y1String, CultureInfo.InvariantCulture) * 2f)
                        });

                        break;

                    case "bead":
                        pattern.Nodes.Add(new Node
                        {
                            PaletteEntry = pattern.Palette[paletteDictionary[Convert.ToInt32(paliindexString)]],
                            Type = NodeType.Bead,
                            X = Convert.ToInt32(Convert.ToSingle(x1String, CultureInfo.InvariantCulture) * 2f),
                            Y = Convert.ToInt32(Convert.ToSingle(y1String, CultureInfo.InvariantCulture) * 2f)
                        });

                        break;

                    default:
                        break;
                }
            }

            return pattern;
        }

        public class Line
        {
            public Vector2 a;
            public Vector2 b;

            public bool AreCodirected(Line line)
            {
                var v1 = b - a;
                var v2 = line.b - line.a;

                v1 = Vector2.Normalize(v1);
                v2 = Vector2.Normalize(v2);

                return Vector2.Dot(v1, v2) > 1 - 0.00001f;
            }
        }

    }
}