﻿using System;

namespace Coterie.Common.Scheme.Format.Oxs
{
    public interface IOxsAdapter
    {
        Pattern ParsePattern(Guid id, byte[] rawdata);
    }
}