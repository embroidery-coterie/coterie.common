﻿namespace Coterie.Common.Scheme
{
    public class Node
	{
		public int X { get; set; }
		public int Y { get; set; }
		public PaletteEntry PaletteEntry { get; set; }
		public NodeType Type { get; set; }
	}		
	
	public enum NodeType
	{
		FrenchKnot,
		Bead
	}
}