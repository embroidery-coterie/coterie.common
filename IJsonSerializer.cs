﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coterie.Common
{
    public interface IJsonSerializer
    {
        T Deserialize<T>(string json);
        string Serialize<T>(T obj);
    }
}