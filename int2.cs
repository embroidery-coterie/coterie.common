﻿namespace Coterie.Common
{
    public struct int2
    {
        public int x;
        public int y;

        public int2(int a, int b)
        {
            x = a;
            y = b;
        }
    }
}

